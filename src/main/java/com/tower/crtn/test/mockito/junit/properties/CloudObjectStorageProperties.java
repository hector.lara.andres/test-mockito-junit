package com.tower.crtn.test.mockito.junit.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@Getter
public class CloudObjectStorageProperties {

  @Value("${properties.cos.bucketName}")
  private String bucketName;

  @Value("${properties.cos.apiKey}")
  private String apiKey;

  @Value("${properties.cos.serverInstanceId}")
  private String serverInstanceId;

  @Value("${properties.cos.endpointUrl}")
  private String endpointUrl;

  @Value("${properties.cos.location}")
  private String location;
}
